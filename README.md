# Cryptng
![](https://gitlab.com/HolyChicken99/cryptng/badges/master/pipeline.svg)

## Hide Data in PNG files ##
## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cryptng` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cryptng, "~> 0.1.0"}
  ]
end
```
## Manual Installation ##
```bash
git clone https://gitlab.com/HolyChicken99/cryptng
cd cryptng
mix deps.get
mix run
```

## Documentation can be found [here](https://cryptng.glitch.me/index.html)
