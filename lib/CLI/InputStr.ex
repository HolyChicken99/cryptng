defmodule CLI.InputStr do
  @moduledoc """
  This is PngRead Module
  """
  @moduledoc since: "1.0.0"
  @spec run(binary() | nil) :: nil | charlist()
  @doc """
  If a String is passed then `input.ex` guarantees that the file is valid and then asks for the string to be encoded in th PNG file.

  Returns on Success: `@String`.
  Returns on Failure: `nil`

  ## Examples

      iex>  Encode.String.run("/tmp/temp.png")
      iex> Enter the string to be encoded : `placeholder text`
      placeholder text



  """
  def run(args), do: askStr(args)

  defp askStr(nil), do: nil

  defp askStr(_val) do
    :stdio
    |> IO.gets("Enter the string to be encoded: ")
    |> String.trim()
    |> String.to_charlist()
  end
end
