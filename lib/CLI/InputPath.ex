defmodule CLI.InputPath do
  @moduledoc """
  This is Input Module
  """
  @moduledoc since: "1.0.0"
  @spec run(any()) :: nil | String.t()
  @spec imagePath(any()) :: nil | String.t()
  @doc """
  Checks if the passed input is a valid file.
  If is a valid file then passes it to the EncStr Module

  Returns on Success: `@String`.
  Returns on Failure: `nil`

  ## Examples

      iex> CLI.Input.run("/?")
      :nil # as there is no path with the given name on a standard unix filesystem

  """

  def run(argv) do
    argv
    |> imagePath
  end

  defp imagePath(""), do: nil

  defp imagePath(argv) do
    if(File.exists?(argv)) do
      IO.puts("file exists #{argv}")
    else
      IO.puts(:stderr, "error")
      nil
    end
  end
end
