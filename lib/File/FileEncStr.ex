defmodule File.EncStr do
  @moduledoc """
  This is String Encoding  Module
  """
  @moduledoc since: "1.0.0"
  @spec run(binary() | nil) :: :ok | nil
  @doc """
  If a String is passed then `input.ex` guarantees that the file is valid and then asks for the string to be encoded in th PNG file.

  Returns on Success: `:ok`.
  Returns on Failure: `nil`

  ## Examples

      iex>  runEncode.String.run("/tmp/temp.png")
      iex> Enter the string to be encoded : `placeholder text`
      placeholder text



  """

  def run(nil), do: nil
  def run(fileName), do: encode(fileName)

  defp encode(filename) do
    filename = filename |> String.trim()

    string =
      IO.gets(" Enter the string to be encoded :")
      |> String.trim()

    IO.puts("the string #{string} will be encoded")
    {status, filedesc} = File.open(filename, [:append])

    case {status, filedesc} do
      {:ok, fileDesc} ->
        File.write!(filename, string, [:binary, :append])

      _ ->
        IO.puts("File not opened")
        nil
    end
  end
end

# IO.gets("enter filename:")
# |> File.EncStr.run()
