import CLI.InputPath, only: [run: 1]
import CLI.InputStr, only: [run: 1]

defmodule Main do
  @moduledoc """
  This is the Entry Point to the main Program
  """
  @moduledoc since: "1.0.0"
  # @doc """
  # Asks user for the PNG file path
  # """

  def start() do
    :stdio
    |> IO.gets("Enter File path")
    |> String.trim()
    |> CLI.InputPath.run()
    |> CLI.InputStr.run()

    # |>
  end
end
